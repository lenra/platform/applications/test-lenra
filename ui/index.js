'use strict'

module.exports =
	function mainUi(data) {
		let result, error;
		try {
			let ui = new Function('data', data.function);
			let dataParsed = JSON.parse(data.data);
			result = ui(dataParsed);
			if (!result.root instanceof Object) throw new Error("The result does not have a correct root property");
		} catch (er) {
			error = er;
		}
		return {
			root: {
				type: "container",
				direction: "col",
				children: [
					{
						type: "container",
						children: [
							{
								type: "container",
								direction: "col",
								children: [
									{
										type: "text",
										value: "data = "
									},
									{
										type: "textfield",
										value: `${data.data}`,
										listeners: {
											onChange: {
												action: "ChangeData"
											}
										}
									}
								]
							},
							{
								type: "container",
								direction: "col",
								children: [
									{
										type: "text",
										value: "function ui(data) {"
									},
									{
										type: "textfield",
										value: `${data.function}`,
										listeners: {
											onChange: {
												action: "ChangeFunction"
											}
										}
									},
									{
										type: "text",
										value: "}"
									},
								]
							}
						]
					},
					{
						type: "container",
						direction: "col",
						children: [
							{
								type: "text",
								value: "Result:"
							},
							error ? {
								type: "text",
								value: `Une erreur s'est produite : ${error}`
							} : result.root
						]
					}
				]
			}
		};
	}